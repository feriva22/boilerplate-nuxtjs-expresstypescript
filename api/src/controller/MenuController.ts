import { JsonController, Param, Body, Get, Post, Put, Delete, QueryParam } from "routing-controllers";
import { GenericError } from "../lib/utils";

@JsonController("/menu")
export class MenuController {
  menuData = [
    { name: "Sate Kambing", description: "Lore ipsmu", price: 50000 },
    { name: "Sate Gurame", description: "Lore ipsmu", price: 60000 },
  ];

  @Get("/")
  getAll() {
    return this.menuData;
  }
}
