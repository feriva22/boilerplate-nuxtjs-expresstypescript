import { JsonController, Param, Body, Get, Post, Put, Delete, QueryParam } from "routing-controllers";
import { GenericError } from "../lib/utils";

@JsonController("/product")
export class ProductController {
  productData = [
    { id: 1, name: "test1" },
    { id: 2, name: "test2" },
  ];

  @Get("/")
  getAll() {
    return this.productData;
  }
}
