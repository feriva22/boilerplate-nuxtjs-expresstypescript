import { JsonController, Param, Body, Get, Post, Put, Delete, QueryParam } from "routing-controllers";
import { getCustomRepository } from "typeorm";
import { GenericError } from "../lib/utils";
import { UserRepository } from "../database/repository/UserRepository";

@JsonController("/auth")
export class AuthController {
  @Post("/login")
  async authenticate(@Body() credential: any) {
    const userRepository = getCustomRepository(UserRepository);
    const user = await userRepository.validatePassword(credential.email, credential.password);

    return {
      token: "hheaa",
      credential: user,
    };
  }
}
