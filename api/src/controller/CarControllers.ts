import { IsEnum } from "class-validator";
import { JsonController, Param, Body, Get, Post, Put, Delete, QueryParam, QueryParams } from "routing-controllers";
import { GenericError } from "../lib/utils";

enum Types {
  Mpv = "mpv",
  City = "city",
  Sedan = "sedan",
  Suv = "suv",
  DoubleCabin = "double cabin",
}

class GetCarsQuery {
  //@IsEnum(Types)
  type?: Types;
}

@JsonController("/cars")
export class CarControllers {
  @Get("/")
  getAll(@QueryParams() query?: GetCarsQuery) {
    console.log(query);
    return {
      message: `Get all Cars`,
      query: query,
      data: [],
    };
  }
}
